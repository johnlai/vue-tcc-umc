
const routes = [
  {
    path: '/',
    component: () => import('layouts/LoginLayout.vue'),
    children: [
      { path: '/', component: () => import('pages/Login.vue'), name: 'Login' },
      { path: '/cadastro', component: () => import('pages/Register.vue'), name: 'Cadastro' },
      { path: '/recuperar_senha', component: () => import('pages/PasswordRecover.vue'), name: 'Recuperar Senha' }
    ]
  },
  {
    path: '/:userid',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '/:userid/dashboard', component: () => import('pages/Dashboard.vue'), name: 'Dashboard', props: true },
      { path: '/:userid/meus_eventos', component: () => import('pages/MyEvents.vue'), name: 'Dashboard', props: true },
      { path: '/:userid/cadastro_usuario', component: () => import('pages/ParticipantRegister.vue'), props: true },
      { path: '/:userid/cadastro_palestrante', component: () => import('pages/SpeakerRegister.vue'), props: true },
      { path: '/:userid/lista_palestrantes', component: () => import('pages/SpeakerList.vue'), props: true },
      { path: '/:userid/criacao_evento', component: () => import('pages/EventRegister.vue'), props: true },
      { path: '/:userid/lista_usuarios', component: () => import('pages/ParticipantList.vue'), props: true },
      { path: '/:userid/criacao_local', component: () => import('pages/LocationRegister.vue'), props: true },
      { path: '/:userid/lista_locais', component: () => import('pages/LocationList.vue'), props: true },
      { path: '/:userid/lista_administradores', component: () => import('pages/AdministratorList.vue'), props: true },
      { path: '/:userid/cadastro_administradores', component: () => import('pages/AdministratorRegister.vue'), props: true },
      { path: '/:userid/lista_coordenadores', component: () => import('pages/CoordinatorList.vue'), props: true },
      { path: '/:userid/cadastro_coordenadores', component: () => import('pages/CoordinatorRegister.vue'), props: true },
      { path: '/:userid/minha_conta', component: () => import('pages/MyAccount.vue'), props: true }
    ],
    props: true
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '*',
    component: () => import('pages/Error404.vue')
  }
]

export default routes
