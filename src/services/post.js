import Api from "./base";
// import { ResponseService } from '@/service/ResponseService'

class PostService extends Api {
  constructor() {
    super("/event");
  }
}
export default new PostService();
