import { Notify } from 'quasar'

const notify = (mensagem = '', cor = '', icone = '', posicao = '') => Notify.create({
  message: mensagem,
  color: cor,
  icon: icone,
  position: posicao
})

export default notify
