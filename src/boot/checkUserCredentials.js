import Vue from 'vue'
import notify from 'src/boot/notify'
import customAxios from 'axios'

export default ({ router }) => {
  const checkUserCredentials = (userRole) => {
    if (userRole === '4' || userRole === '3') {
      router.push(`/${localStorage.getItem('userId')}/dashboard`)
      notify('Parece que você não tem permissão para acessar está pagina!', 'negative', 'warning', 'top')
    }
  }

  const checkAdmCredentials = (userRole) => {
    if (userRole !== '1') {
      router.push(`/${localStorage.getItem('userId')}/dashboard`)
      notify('Parece que você não tem permissão para acessar está pagina!', 'negative', 'warning', 'top')
    }
  }

  const checkUserToken = () => {
    customAxios.get(`${process.env.API}/auth/current`, {
      headers: JSON.parse(localStorage.getItem('acessToken'))
    }).then(response => {
      console.log('tudo ok')
      localStorage.setItem('userRole', response.data.role)
      localStorage.setItem('userId', response.data.identity)
    }).catch(er => {
      if (er.response.status === 401) {
        notify('Oops, sua sessão expirou! Tente logar novamente.', 'negative', 'warning', 'bottom')
        // if the request fails, remove any possible user token
        localStorage.removeItem('acessToken')
        localStorage.removeItem('userId')
        localStorage.removeItem('userRole')
        this.$router.push('/')
      } else {
        notify('Oops, aconteceu alguma coisa errada!', 'negative', 'warning', 'bottom')
      }
    })
  }

  Vue.prototype.$checkUserCredentials = checkUserCredentials
  Vue.prototype.$checkUserToken = checkUserToken
  Vue.prototype.$checkAdmCredentials = checkAdmCredentials
}
