import Vue from 'vue'
import axios from 'axios'

const customAxios = axios.create({
  baseURL: `${process.env.API}`
})

Vue.prototype.$axios = customAxios

export { customAxios }
